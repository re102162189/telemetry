##### build spring boot jar
```
mvn clean package
```

##### build/start all containers
```
docker-compose build
docker-compose up -d
```

##### sending request to spring boot
```
curl http://localhost:8080/api/users/getUsers
curl http://localhost:8080/api/users/1
```

##### checking Kibana
```
http://localhost:5601/
```

##### get es cluster uuid and add to config
```
GET http://localhost:9200/_cluster/state
```

##### checking prometheus
```
http://localhost:9090
```

##### checking grafana
```
http://localhost:3000
```

##### start/stop/restart/remove containers
```
docker-compose start
docker-compose stop
docker-compose restart
docker-compose down
```

##### remove docker volume
```
docker volume ls
docker volume rm {volume_id}
```